import os
import skimage
import numpy as np
import matplotlib.pyplot as plt
from skimage import transform
from skimage.color import rgb2gray
import tensorflow as tf
import random

# define load_data method
def load_data(data_directory):
    directories = [d for d in os.listdir(data_directory) 
                  if os.path.isdir(os.path.join(data_directory,d))]
    labels = []
    images = []
    
    for d in directories:
        label_directory = os.path.join(data_directory, d)
        file_names = [os.path.join(label_directory, f)
                      for f in os.listdir(label_directory)
                      if f.endswith(".ppm")]
        
        for f in file_names:
            images.append(skimage.data.imread(f))
            labels.append(int(d))
            
    return images, labels

# define filt path
ROOT_PATH = "D:/Learning/TensorFlow/traffic_sign/"
train_data_directory = os.path.join(ROOT_PATH, "BelgiumTSC_Training/Training")
test_data_directory = os.path.join(ROOT_PATH, "BelgiumTSC_Testing/Testing")

#print(train_data_directory)
#print(test_data_directory)
#■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
# load training data
train_raw_images, train_raw_labels = load_data(train_data_directory)

# convert raw data to array
train_images = np.array(train_raw_images)
train_labels = np.array(train_raw_labels)

#print size, 
print("images shape: {0}, ndim: {1}, size: {2}"
      .format(train_images.shape,
              train_images.ndim,
              train_images.size))

print("labels shape: {0}, ndim: {1}, size: {2}"
      .format(train_labels.shape,
              train_labels.ndim,
              train_labels.size))
#■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
# get some images and show to see how they are look like
pick_traffic_signs = [300, 2250, 3650, 4000]
"""
# fill out the subplots with the random images that you defined
plt.figure(figsize = (7, 7))
for i in range(len(pick_traffic_signs)):
    # chia screen thanh 1 row, 4 columns, i+1: image print position
    #plt.subplots(1, 4, i+1)
    plt.subplot(4, 1, i+1)
    plt.axis("off") # no  axis
    plt.imshow(train_raw_images[pick_traffic_signs[i]])
    plt.subplots_adjust(wspace = 0.5)
    plt.title("shape: {0}, min: {1}, max: {2}"
          .format(train_raw_images[pick_traffic_signs[i]].shape,
                  train_raw_images[pick_traffic_signs[i]].min(),
                  train_raw_images[pick_traffic_signs[i]].max()))
"""
#■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
# because image size is not the same size => rescale images (28, 28)
print('because image size is not the same size => rescale images (28, 28)')
#rescale the images in the images arrays
images28 = [transform.resize(image, (28, 28)) for image in train_raw_images]
"""
#show images
plt.figure(figsize = (7, 7))
for i in range(len(pick_traffic_signs)):
    # chia screen thanh 1 row, 4 columns, i+1: image print position
    #plt.subplots(1, 4, i+1)
    plt.subplot(4, 1, i+1)
    plt.axis("off") # no  axis
    plt.imshow(images28[pick_traffic_signs[i]])
    plt.subplots_adjust(wspace = 0.5)
    plt.title("shape: {0}, min: {1}, max: {2}"
          .format(images28[pick_traffic_signs[i]].shape,
                  images28[pick_traffic_signs[i]].min(),
                  images28[pick_traffic_signs[i]].max()))
"""
#■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
# trying to answer a classification question => the color in the pictures matters less    
# convert to grayscale
images28 = np.array(images28)
gray_images28 = rgb2gray(images28)  
"""
 #show images
plt.figure(figsize = (7, 7))
for i in range(len(pick_traffic_signs)):
    # chia screen thanh 1 row, 4 columns, i+1: image print position
    #plt.subplots(1, 4, i+1)
    plt.subplot(4, 1, i+1)
    plt.axis("off") # no  axis
    plt.imshow(gray_images28[pick_traffic_signs[i]], cmap="gray")
    plt.subplots_adjust(wspace = 0.5)
    plt.title("shape: {0}, min: {1}, max: {2}"
          .format(gray_images28[pick_traffic_signs[i]].shape,
                  gray_images28[pick_traffic_signs[i]].min(),
                  gray_images28[pick_traffic_signs[i]].max()))
"""
#■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
#tensorflow
# initialize placeholders
x = tf.placeholder(dtype = tf.float32, shape = [None, 28, 28])
y = tf.placeholder(dtype = tf.int32, shape = [None])

# fltten the input data (28, 28) => 784
images_flat = tf.contrib.layers.flatten(x)

# fully connected layer
# 62: output
logits = tf.contrib.layers.fully_connected(images_flat, 62, tf.nn.relu)

#define loss
loss = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(
        labels = y, logits = logits))

# define an optimizer
train_op = tf.train.AdamOptimizer(learning_rate=0.001).minimize(loss)

# convert logits to label indeser
correct_pred = tf.argmax(logits, 1)

# define an accuracy metric
arruracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))    
    
print("images_flat: ", images_flat)
print("logits: ", logits)
print("loss: ", loss)
print("predicted_labels: ", correct_pred)

#■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
#training data 
tf.set_random_seed(1234)
sess = tf.Session()

sess.run(tf.global_variables_initializer())
# training by loop 201 times
for i in range(201):
    print("EPOCH: ", i)
    _, accuracy_val = sess.run([train_op, arruracy], 
                               feed_dict={x: gray_images28, 
                                          y: train_raw_labels})
    if i % 10 == 0:
        print("loss: ", loss)
    
    print("accuracy_val: ", accuracy_val)
    print("DONE Widht EPOCH")
    
#■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
#pick 10 random images
sample_indexes = random.sample(range(len(gray_images28)), 10)
sample_images = [gray_images28[i] for i in sample_indexes]
sample_labels = [train_raw_labels[i] for i in sample_indexes]
# run the "correct_pred" operation
predicted = sess.run([correct_pred], feed_dict={x: sample_images})[0]

print(sample_labels)
print(predicted)
"""
# display the predictio ns and the ground truth visually
for i in range(len(sample_indexes)):
    plt.subplot(5, 2, i + 1)
    plt.axis("off")
    plt.imshow(sample_images[i], cmap="gray")
    plt.subplots_adjust(wspace=0.5)
    color = "green" if sample_labels[i] == predicted[i] else "red"
    plt.text(40, 10, "Truth: {0} \n Prediction: {1}"
             .format(sample_labels[i], predicted[i]),
             fontsize=12, color = color)
"""    
#■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
print("use testing data to check")
# use test data to check predict
# load data
test_data_directory = os.path.join(ROOT_PATH, "BelgiumTSC_Testing/Testing")
test_images, test_labels = load_data(test_data_directory)

#rescale
test_images28 = [transform.resize(image, (28, 28)) for image in test_images]

#convert to grayscale
test_images28 = rgb2gray(np.array(test_images28))

#predited
test_predited =  sess.run([correct_pred], feed_dict={x: test_images28})[0]

# calculate correct matches
match_count = sum([int(y == y_) for y, y_ in zip(test_labels, test_predited)])

# calculate the accuracy
test_accuracy = match_count / len(test_labels)

print("Accuracy: {:.3f}".format(test_accuracy))